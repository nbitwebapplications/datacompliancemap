class DataMapClient{
    constructor(){
		this.handlers = {};
		this.state = null;
		this.templates = {};
		this.documentReady = false;
		this.lastEvent = false;
		
		$(document).ready((function(){
			this.documentReady = true;
			this.loadTemplates();
			this.fireEvent("document-ready");
		}).bind(this));
	}
	on(h, fn)
	{
		var handler = this.handlers[h];
		if(!handler)
		{
			this.handlers[h] = [fn];
		}
		else
		{
			if(this.handlers[h].indexOf(fn) == -1)
			{
				this.handlers[h].push(fn);
			}
		}
	}
	fireEvent(ename, data)
	{
		console.log(ename);
		if(this.handlers[ename])
		{
			for(var h = 0; h < this.handlers[ename].length; h++)
			{
				this.handlers[ename][h](data);
			}
		}
		else if(this.handlers[this.state + "." + ename])
		{
			for(var h = 0; this.handlers[this.state + "." + ename] && h < this.handlers[this.state + "." + ename].length; h++)
			{
				this.handlers[this.state + "." + ename][h](data);
			}
		}
		this.lastEvent = ename;
	}
	setState(state)
	{
		if(this.state)
		{
			this.fireEvent("state-end", this.state);
			this.state = null;
		}
		this.state = state;
		
		if(this.stateChanged == undefined)
			this.stateChanged = false;
		else
			this.stateChanged = true;
		
		this.fireEvent("state-start", this.state);
	}
	loadTemplates()
	{
		var templates = $(".template");
		for(var t = templates.length - 1; t >= 0; t--)
		{
			var template = templates[t];
			$(template).removeClass("template");
			var tid = $(template).attr("id");
			this.templates[tid] = {parent:$(template).parent(), html:template.outerHTML};
			$(template).remove();
		}
	}
	render(arr, template)
	{
		if(!Array.isArray(arr))
			arr = [arr];
		var retstr = "";
		for(var a = 0; a < arr.length; a++)
		{
			var str = this.mapObjToTemplate(arr[a], this.templates[template].html);
			if(arr[a]._id)
				str = $(str).attr("id", arr[a]._id)[0].outerHTML;
			retstr += str;
		}
		return retstr;
	}
	getTemplateParent(template)
	{
		return this.templates[template].parent;
	}
	mapObjToTemplate(obj, template)
	{
		var re = /\{\{([A-Za-z_]+)\}\}/; 
		var m;
	
		while ((m = re.exec(template)) != null) {
			var replacement = obj[m[1]];
			if(!replacement)
				replacement = "";
			template = template.replace(m[0], replacement, "g");
		}
		return template;
	}
	loadSystems(){
		var dt = new Date();
		$.jget(_spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('Systems')/items?$select=Id,Title,Comments,Intracompany" + "&ts=" + dt.getTime(), (function(result){
			this.app.fireEvent("systems-loaded", result.d.results);
		}).bind({app: this}));
	}
	loadProperties(){
		var dt = new Date();
		$.jget(_spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('Properties')/items" + "?ts=" + dt.getTime(), (function(result){
			this.app.fireEvent("properties-loaded", result.d.results);
		}).bind({app: this}));

	}
	loadTransfers(propid){
		var dt = new Date();
		$.jget(_spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('Transfers')/items?$select=Id,FromSystemId,ToSystemId,Action&$filter=PropertyId eq " + propid + "&ts=" + dt.getTime(), (function(result){
			this.app.fireEvent("transfers-loaded", result.d.results);
		}).bind({app: this}));

	}
	loadRequests(propid){
		var dt = new Date();
		var lastweek = new Date(new Date().setDate(dt.getDate() - 7));
		$.jget(_spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('Requests')/items?$select=Id,Title,Status&$filter=PropertyId eq " + propid + " and (Status ne 'closed' or Modified gt datetime'" + lastweek.toISOString() + "')&$orderBy=Id desc&ts=" + dt.getTime(), (function(result){
			this.app.fireEvent("requests-loaded", result.d.results);
		}).bind({app: this}));
	}
	loadRequest(reqid){
		var dt = new Date();
		$.jget(_spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('Requests')/items?$select=Id,Title,Log,Status,Created,ReceivedOn,CompletedOn&$filter=ID eq " + reqid + "&ts=" + dt.getTime(), (function(result){
			var req = result.d.results[0];
			var dt = new Date();
			$.jget(_spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('RequestLog')/items?$select=Id,Title,Created&$filter=RequestId eq " + req.Id + "&$orderBy=Created asc&ts=" + dt.getTime(), (function(result){
				var req = this.req;
				req.logs = result.d.results;
				this.app.fireEvent("request-loaded", req);
			}).bind({app: this.app, req:req}));
		}).bind({app: this}));
	}
	loadTasks(reqid){
		var dt = new Date();
		$.jget(_spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('TaskReferences')/items?$select=Id,Title,SystemId,IsComplete&$filter=RequestId eq " + reqid + "&ts=" + dt.getTime(), (function(result){
			this.app.fireEvent("tasks-loaded", result.d.results);
		}).bind({app: this}));
	}
	loadDecision(decid){
		var dt = new Date();
		$.jget(_spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('TaskReferences')/items?$select=Id,Title,SystemId,IsComplete,Decision&$filter=ID eq " + decid + "&ts=" + dt.getTime(), (function(result){
			$.jget(_spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('Systems')/items?$Title&$filter=ID eq " + result.d.results[0].SystemId + "&ts=" + dt.getTime(), (function(result){
				this.dec.DecisionTitle = result.d.results[0].Title;
				this.app.fireEvent("decision-loaded", this.dec);
			}).bind({app: this.app, dec: result.d.results[0]}));
		}).bind({app: this}));
	}
	loadDecisionOptions(dec)
	{
		var dt = new Date();
		$.jget(_spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('Transfers')/items?$select=Id,FromSystemId,ToSystemId,Action&$filter=FromSystemId eq " + dec.SystemId + "&ts=" + dt.getTime(), (function(result){
			this.app.fireEvent("options-loaded", result.d.results);
		}).bind({app: this}));
	}
	setDecision(decid, dectitle, decval)
	{
		$.jupdateItem("TaskReferences", decid, dectitle, {"Decision": decval}, (function(result){
			this.app.fireEvent("decision-updated");
		}).bind({app: this}));
	}
}

$.extend({
	jget: function(url, callback){
		return $.ajax({
			type: 'GET',
			url: url,
			headers: { "ACCEPT": "application/json;odata=verbose" },
			success: function (data) {
				console.log(data)
			},
			error: function () {
				//alert("Failed to get details");
			}
		}).done(callback);
	},
	jupdateItem: function(list, id, title, body, callback) {
		body.Title = title;
		var strbody = JSON.stringify(body);
		var url = _spPageContextInfo.webAbsoluteUrl + "/_api/web/Lists/GetByTitle('" + list + "')/items(" + id + ")";
		return $.ajax({
			type: 'POST',
			headers: { 
				"ACCEPT": "application/json;odata=verbose",
				"X-RequestDigest":$("#__REQUESTDIGEST").val(),
				"X-HTTP-Method": "MERGE",
				"IF-MATCH":"*"
			},
			url: url,
			data: strbody,
			contentType: "application/json",
			dataType: 'json'
		}).done(callback);
	},
	jput: function(url, body, callback) {
		return $.ajax({
			type: 'PUT',
			url: url,
			data: JSON.stringify(body),
			contentType: "application/json",
			dataType: 'json'
		}).done(callback);
	},
	jdelete: function(url, callback) {
		return $.ajax({
			type: 'DELETE',
			url: url,
			contentType: "application/json",
			dataType: 'json'
		}).done(callback);
	}
});