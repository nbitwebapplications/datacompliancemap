var mongoClient = require("mongodb").MongoClient;
var express = require('express');
var app = express();
var mongodb = null;
var ObjectID = require("mongodb").ObjectID;
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.use(express.static('include'));

mongoClient.connect("mongodb://nb-info-dependency:GUX6m3aXrMJ0jRNPwVmm691MMGGAxKelpHn9yAu7NibN34vOXw439DyAZGLEbAIKTwYbJObIcdaeGF5xBxRHrw==@nb-info-dependency.mongo.cosmos.azure.com:10255/?ssl=true&appName=@nb-info-dependency@", function (err, client) {
    console.log("OK");
    mongodb = client.db('admin');
});

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/home.html');
});
app.get('/systems', function(req, res){
    mongoGet("Systems", {}, (function(err, data){
        if(err)
        {
            this.res.send({status:"ERROR", error:err});
        }
        else
        {
            this.res.send({status:"OK", data});
        }
    }).bind({res:res}));
});
app.get('/properties/:propid/transfers', function(req, res){
    var propid = new ObjectID(req.params.propid);
    mongoGet("Transfers", {propertyId:propid}, (function(err, data){
        if(err)
        {
            this.res.send({status:"ERROR", error:err});
        }
        else
        {
            this.res.send({status:"OK", data});
        }
    }).bind({res:res}));
});
app.post('/systems', function(req, res){
    var nsystem = req.body;
    console.log("/systems");
    console.log(req.body);
    mongoInsert("Systems", req.body, (function(err, data){
        if(err)
        {
            this.res.send({status:"ERROR", error:err});
        }
        else
        {
            this.res.send({status:"OK", data});
        }
    }).bind({res:res, system:nsystem}));
});

app.get('/properties', function(req, res){
    mongoGet("Properties", {}, (function(err, data){
        if(err)
        {
            this.res.send({status:"ERROR", error:err});
        }
        else
        {
            this.res.send({status:"OK", data});
        }
    }).bind({res:res}));
});

app.listen(3000, function () {
    console.log('Data Compliance Mapp listening on port 3000!');
});

// MONGO

function mongoGet(collection, query, fn)
{
	mongodb.collection(collection).find(query).toArray(fn);
}

function mongoInsert(collection, record, fn)
{
	mongodb.collection(collection).insert(record, null, fn);
}

function mongoDelete(collection, id, fn)
{
	// check to see if user is member of team
	// OR session.teamid = 
	var filter = {_id: new mongo.ObjectID(id)};
	mongodb.collection(collection).remove(filter, fn);
}

function mongoUpdate(collection, id, update, fn)
{
	console.log("MONGO UPDATE " + id);
	var query = {_id: new mongo.ObjectID(id)};
	if(update["$addToSet"])
	{
		var nupdate = {};
		nupdate["$addToSet"] = update["$addToSet"];
		delete update["$addToSet"];
		update.updated_on = new Date();
		nupdate["$set"] = update;
		update = nupdate;
	}
	else if(update["$inc"])
	{
		var nupdate = {};
		nupdate["$inc"] = update["$inc"];
		delete update["$inc"];
		update.updated_on = new Date();
		nupdate["$set"] = update;
		update = nupdate;
	}
	else
	{
		update.updated_on = new Date();
		update = { $set: update };
	}
	console.log(update);
	mongodb.collection(collection).update(query, update, fn);
}

function mongoUpdateMulti(collection, query, update, fn)
{
	console.log("MONGO UPDATE " + JSON.stringify(query));
	mongodb.collection(collection).update(query, update, {multi:true}, fn);
}

function mongoCount(collection, filter, fn)
{
	mongodb.collection(collection).count(filter, fn);
}
